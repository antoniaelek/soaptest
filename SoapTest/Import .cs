﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Services.Description;
using System.Xml;
using System.Xml.Serialization;

namespace SoapTest
{
    public class Program
    {
        public static void Main()
        {
            var url = "https://www.crcind.com/csp/samples/SOAP.Demo.CLS?WSDL=1";
            var serviceName = "SOAPDemo";
            var operationName = "AddInteger";

            var service = new Service(url);
            var method = service.GetMethod(serviceName, operationName);
            var parameters = method.GetParameters();
            var x = service.Invoke(serviceName, operationName, 1, true, 2, true);
            //var service = new Service("http://demo.borland.com/orderwebservice/orderservice.asmx?WSDL");
            //var method = service.GetMethod("OrderService", "GetOrders");
            //var parameters = method?.GetParameters();
            //var result = service?.Invoke("OrderService", "GetOrders");
        }
    }
    public class Service
    {
        private readonly Assembly _assembly;

        public dynamic Invoke(string serviceName, string methodName, params object[] parameters)
        {
            var service = GetServiceInstance(serviceName);
            if (service is null)
            {
                return null;
            }

            var method = GetMethod(service, methodName);

            return method?.Invoke(service, parameters);
        }

        public MethodInfo GetMethod(string serviceName, string methodName)
        {
            var service = GetServiceInstance(serviceName);
            if (service is null)
            {
                return null;
            }

            return GetMethod(service, methodName);
        }

        private static MethodInfo GetMethod(object serviceInstance, string methodName)
        {
            if (string.IsNullOrWhiteSpace(methodName))
            {
                return null;
            }

            return serviceInstance?.GetType().GetMethods().FirstOrDefault(method => methodName.Equals(method.Name));
        }

        private object GetServiceInstance(string serviceName)
        {
            if (string.IsNullOrWhiteSpace(serviceName))
            {
                return null;
            }

            return _assembly?.CreateInstance(serviceName);
        }

        public Service(string url)
        {
            // Get a WSDL file describing a service.
            var xmlreader = new XmlTextReader(url);
            var description = ServiceDescription.Read(xmlreader);

            // Initialize a service description importer.
            var importer = new ServiceDescriptionImporter
            {
                //ProtocolName = "Soap12"  // Use SOAP 1.2.
            };
            importer.AddServiceDescription(description, null, null);

            // Report on the service descriptions.
            Console.WriteLine("Importing {0} service descriptions with {1} associated schemas.", importer.ServiceDescriptions.Count, importer.Schemas.Count);

            // Generate a proxy client.
            importer.Style = ServiceDescriptionImportStyle.Client;

            // Generate properties to represent primitive values.
            importer.CodeGenerationOptions = CodeGenerationOptions.GenerateProperties;

            // Initialize a Code-DOM tree into which we will import the service.
            var nmspace = new CodeNamespace();
            var unit = new CodeCompileUnit();
            unit.Namespaces.Add(nmspace);

            // Import the service into the Code-DOM tree. This creates proxy code that uses the service.
            var warning = importer.Import(nmspace, unit);
            if (warning != 0)
            {
                throw new Exception(warning.ToString());
            }

            // Generate and print the proxy code in C#.
            //CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");
            //provider.GenerateCodeFromCompileUnit(unit, Console.Out, new CodeGeneratorOptions());

            //set the CodeDOMProvider to C# to generate the code in C#
            var sw = new StringWriter();
            var provider = CodeDomProvider.CreateProvider("C#");
            provider.GenerateCodeFromCompileUnit(unit, sw, new CodeGeneratorOptions());

            // Creating TempFileCollection
            var coll = new TempFileCollection
            {
                KeepFiles = false
            };

            //setting the CompilerParameters for the temporary assembly
            string[] refAssembly = { "System.dll", "System.Data.dll", "System.Web.Services.dll", "System.Xml.dll" };
            var param = new CompilerParameters(refAssembly)
            {
                GenerateInMemory = true,
                TreatWarningsAsErrors = false,
                OutputAssembly = "WebServiceReflector.dll",
                TempFiles = coll
            };

            //compile the generated code into an assembly
            //CompilerResults results = provider.CompileAssemblyFromDom(param, unitArr);
            var results = provider.CompileAssemblyFromSource(param, sw.ToString());
            _assembly = results.CompiledAssembly;
        }
    }
}